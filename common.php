<?php // Stan 2004-08-09

/**
* @package      PHP_Common
* @version      0.26
* @date         2011-02-16
*
* @author       Stan Ovchinnikov
* @email        lishnih@gmail.com
* @copyright    2004—2011
*/

// Скрипт объявляет глобальные переменные:
// $phpcommon_errors
// $phpcommon_old_error_handler

// Функции:
// phpCommonExitHandler phpCommonErrorHandler
// print_rb print_ra  print_rt print_sh
//          sprint_ra sprint_rt

// И следующие константы:
   define( 'PHP_COMMON', dirname( __FILE__ ) );     // Вход с скрипт
   define( 'ERROR',      E_USER_ERROR        );     // Ошибка
   define( 'WARNING',    E_USER_WARNING      );     // Предупреждение
   define( 'NOTICE',     E_USER_NOTICE       );     // Диалог пользователя

// define( 'PHPCOMMON_ERRORSMAILTO',  'root@localhost' );
   define( 'PHPCOMMON_ERRORSMAILFLAG', E_ALL ^ E_USER_NOTICE );
// Если произойдёт любая из указанных ошибок (PHPCOMMON_ERRORSMAILFLAG),
// то будет отправлен отчёт получателю (PHPCOMMON_ERRORSMAILTO)
// Закомментируйте мыло или флаги, если не хотите получать отчёты

// Некоторые параметры для скрипта передаются через константы
// PHPCOMMON_ALWAYSEXPANDEDSTRING - Обработчик ошибок выведет развёрнутый текст

// ob_start( 'ob_gzhandler' );      // Добавляем сжатие страницы
   error_reporting ( E_ALL );       // Ошибки, которые будут выводиться на экран

////////////////////////////////////////////////////////////////////////////////
//////// Установить функцию, которая выполняется по завершению процесса ////////
////////////////////////////////////////////////////////////////////////////////
// Эта функция объявлена для того чтобы она пыталась отправить все
// накопившиеся по ходу выполнения сообщения об ошибках на мыло
// при нормальном завершении переменная $phpcommon_errors пуста
if ( defined( 'PHPCOMMON_ERRORSMAILTO' ) and defined( 'PHPCOMMON_ERRORSMAILFLAG' ) ) {
  function phpCommonExitHandler ( ) {
  global $phpcommon_errors;
    if ( $phpcommon_errors ) {  // Если есть что отсылать
      // Определяем, нужно ли отправлять отчёт
      $need_to_send = 0;
      foreach( $phpcommon_errors as $error )
        if ( $error['ErrNo'] & PHPCOMMON_ERRORSMAILFLAG )
          $need_to_send = 1;
      if ( $need_to_send ) {
        $host = isset( $_SERVER['HTTP_HOST']   ) ? $_SERVER['HTTP_HOST']   : 'null';
        $uri  = isset( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : 'null';
        $subject = 'PHP_Common [' . $host . '] (' . date( 'd.m.Y H:i O' ) . ")\n";
        $message = 'http://' . $host . $uri . "\n" . sprint_rt( $phpcommon_errors );
        $headers = "MIME-Version: 1.0\r\n" .
                   "Content-Transfer-Encoding: 8bit\r\n" .
                   "Content-Type: text/html; charset=utf-8\r\n" .
                   'X-Mailer: PHP/' . phpversion();
        return mail( PHPCOMMON_ERRORSMAILTO, $subject, $message, $headers );
      }; // if
    }; // if
  } // function
  register_shutdown_function( 'phpCommonExitHandler' );
} // if

//////////////////////////////////////////////////////
//////// Устанавливаем свой обработчик ошибок ////////
//////////////////////////////////////////////////////
//         E_ALL               Все предупреждения и ошибки.
//     1 ! E_ERROR             Критические ошибки времени выполнения.
//     2   E_WARNING           Предупреждения времени выполнения.
//     4 ! E_PARSE             Ошибки трансляции.
//     8   E_NOTICE            Замечания времени выполнения (это такие предупреждения, которые,
//                             скорее всего, свидетельствуют о логических ошибках в сценарии,
//                             например, использовании, неинициализированной переменной).
//    16 ! E_CORE_ERROR        Критические ошибки в момент старта PHP.           ( PHP 4 )
//    32 ! E_CORE_WARNING      Некритические предупреждения во время старта PHP. ( PHP 4 )
//    64 ! E_COMPILE_ERROR     Критические ошибки времени трансляции.            ( PHP 4 )
//   128 ! E_COMPILE_WARNING   Предупреждения времени трансляции.                ( PHP 4 )
//   256   E_USER_ERROR        Сгенерированные пользователем ошибки.             ( PHP 4 )
//   512   E_USER_WARNING      Сгенерированные пользователем предупреждения.     ( PHP 4 )
//  1024   E_USER_NOTICE       Сгенерированные пользователем уведомления.        ( PHP 4 )
//  2048 ~ E_STRICT            Enable to have PHP suggest changes to your code   ( PHP 5 )
//                             which will ensure the best interoperability and
//                             forward compatibility of your code.
//  4096   E_RECOVERABLE_ERROR Catchable fatal error                             ( 5.2.0 )
//  8192   E_DEPRECATED        Run-time notices. Enable this to receive warnings ( 5.3.0 )
//                             about code that will not work in future versions.
// 16384   E_USER_DEPRECATED   User-generated warning message.                   ( 5.3.0 )
//       ^ - ошибки, которые не вызывают пользовательскую функцию обработчика ошибок
function phpCommonErrorHandler ( $errno, $errstr, $errfile, $errline ) {
global $phpcommon_errors;
  // Cкрываем путь скрипта от пользователя
  $errfile = basename( $errfile );
  // Любую ошибку заносим в массив ошибок
  // Этот массив будет выводится при возникновении E_USER_ERROR
  if ( !error_reporting() ) {   // скрытая ошибка ( @команда ) - не выводим
    $phpcommon_errors[] = array( 'ErrNo' => $errno, 'ErrStr' => '@[ ' . $errstr . ' ]',
                                 'ErrFile' => $errfile, 'ErrLine' => $errline );
    // Если error_reporting установить в ноль, то E_USER_ERROR (в случае возникновения)
    // не будет получать управление - делаем дополнительную проверку сами
    if ( $errno & E_USER_ERROR ) exit();
  } else
    $phpcommon_errors[] = array( 'ErrNo' => $errno, 'ErrStr' => $errstr,
                                 'ErrFile' => $errfile, 'ErrLine' => $errline );

  switch ( $errno ) {
  // Все четыре последующие варианта просто выведут маркер ошибки
  // Просто я ещё не придумал, чем различить E_USER_WARNING от E_USER_NOTICE
    case E_NOTICE:
      $errtype = 'E_NOTICE';  $marker = '(N)'; $color = 'blue'; break;
    case E_WARNING:
      $errtype = 'E_WARNING'; $marker = '(W)'; $color = 'red';  break;
    case E_USER_NOTICE:
      $errtype = 'NOTICE';    $marker = '(n)'; $color = 'blue'; break;
    case E_USER_WARNING:
      $errtype = 'WARNING';   $marker = '(w)'; $color = 'red';  break;
    case E_USER_ERROR:  // Это фатальная ошибка - выполнение скрипта прекратится
      @ob_end_clean();  // Ничего выводится на броузер не будет, кроме описания ошибки
      print_rt( $phpcommon_errors );
      exit();
    default:
      $errtype = $errno; $marker = '()'; $color = 'orange';
      break;
  }; // switch

  // Разбираемся с цветами
  $bgcolor = 'black';
  if ( preg_match( '/^(\+?)(\{([\#\d\w]*)\|?([\#\d\w]*)\})?(.+)/is', $errstr, $matches ) ) {
  //                  $1 +, $2 {...}, $3 color $4 bgcolor  $5 Сообщение
    $errstr = $matches[5];
    if ( $matches[1] or defined( 'PHPCOMMON_ALWAYSEXPANDEDSTRING' ) ) {
      $marker = $errstr . "<br />\n";
      $errstr = '';
      $bgcolor = '';
    }; // if
    if ( $matches[3] )      $color   = $matches[3];
    if ( $matches[4] )      $bgcolor = $matches[4];
    if ( $matches[2] and !$matches[3] and !$matches[4] )
      $color = $bgcolor = '';   // Если скобочки пустые {}, то сбрасываем цвета
  }; // if
  switch ( true ) {
    case $color and $bgcolor:   // Заданы оба цвета
      $style = 'style="color:' . $color . '; background-color:' . $bgcolor . '"';
      break;
    case $color:                // Задан только Color
      $style = 'style="color:' . $color . '"';
      break;
    case $bgcolor:              // Задан только BgColor
      $style = 'style="background-color:' . $bgcolor . '"';
      break;
    default:
      $style = '';
  }; // switch

  // Выводим на экран требуемые ошибки и предупреждения
  if ( error_reporting() & $errno )
    echo '<span ' . $style . ' title="' .
         htmlspecialchars( "[ $errtype ] $errstr [ $errfile:$errline ]" ) .
         '">' . $marker . "</span>\n";
} // function
// Создаём массив для хранения инфы об ошибках
$phpcommon_errors = array();
// сохраняем стандартный обработчик ошибок
$phpcommon_old_error_handler = set_error_handler( 'phpCommonErrorHandler' );

/////////////////////////////////////////
//////// Функции вывода массивов ////////
/////////////////////////////////////////
// Эта функция подобна вызывает print_ra для всех переданных аргументов
function print_rb () {
  echo '<table border="1">';
  for ( $i = 0; $i < func_num_args(); $i++ ) {
    echo "\n<tr><td>$i<td>";
    print_ra( func_get_arg( $i ) );
  }; // for
  echo "\n</table>\n";
} // function

// Обёртка к функции print_r
function print_ra ( $array, $params = null ) {
  echo sprint_ra( $array, $params );
} // function

function sprint_ra ( $array, $params = null ) {
  if ( $array === False ) return '<i>False</i>';
  if ( $array === True  ) return '<i>True</i>';
  if ( $array === NULL  ) return '<i>NULL</i>';
  if ( $array === ''    ) return '<i>empty</i>';

  if ( is_numeric( $params ) )
    $max_string = (int) $params;
  elseif ( is_string( $params ) )
    $params = explode( ',', $params );

  if ( is_object( $array ) or is_array( $array ) ) {
    $content = '<table border="1">';
    foreach( $array as $key => $val ) {
      $content .= "\n <tr><td>$key<td>";
      $content .= sprint_ra( $val, $params );
    }; // foreach
    $content .= "\n</table>\n";
  } elseif ( is_string( $array ) ) {
    if ( is_array( $params ) )
      foreach( $params as $val ) {
        if     ( is_numeric( $val )  ) $max_string = (int) $val;
        elseif ( $val == 'htmlchars' ) $htmlchars = true;
        elseif ( $val == 'strip'     ) $strip = true;
        elseif ( $val == 'nopre'     ) $nopre = true;
      }; // foreach
    if ( isset( $htmlchars ) )
      $array = htmlspecialchars( $array );
    if ( isset( $strip ) )
      $array = strip_tags( $array );
    if ( isset( $max_string ) and strlen( $array ) > $max_string ) {
      $array = strip_tags( $array );
      $array = substr( $array, 0, $max_string ) . '<span title="' . strip_tags( $array ) . '">...</span>';
    }; // if
    $content = isset( $nopre ) ? $array : "<pre>$array</pre>";
  } else
    $content = $array;
  return $content;
} // function

// Эта функция выводит структурированные массивы в виде таблицы
// действительно только для массивов вида array( 0 => array(), 1 => array() )
function print_rt ( $array ) {
  echo sprint_rt( $array );
} // function

function sprint_rt ( $array ) {
  if ( !is_array( $array ) ) return '<i>must be array</i>';
  if ( $array === ''       ) return '<i>empty</i>';

  if ( !is_array( current( $array ) ) )
    $array = array( '' => $array );
  $content = '<table border="1">
<tr>';
  $a_keys = array_keys( current( $array ) );
  $content .= "\n <th>#";
  foreach( $a_keys as $val )
    $content .= "\n <th>$val";
  foreach( $array as $key => $val ) {
    $content .= "\n<tr>\n <td><i>$key</i>";
    foreach( $val as $val )
      $content .= "\n <td>" . sprint_ra( $val, 'nopre' );
  }; // foreach
  $content .= "\n</table>\n";
  return $content;
} // function

//////////////////////////////////////
//////// Функции вывода строк ////////
//////////////////////////////////////
// Эта функция выводит hex-коды символов строки
function print_sh ( $str, $c = 16 ) {
    echo '<table border="1">
<tr><td><table border="1" cellpadding="3">';
    for ( $i = 0; $i < strlen( $str ); $i++ ) {
      if ( $i % $c == 0 )
        echo "\n<tr>";
      echo '<td>' . sprintf( '%02X', ord( $str[$i] ) );
    }; // for
    echo '</table>
<td><table border="1" cellpadding="3">';

  if ( function_exists( 'mb_detect_encoding' ) ) {
    $ec = mb_detect_encoding( $str );
    echo "\n<tr>";
    for ( $i = $j = 0; $i < mb_strlen( $str, $ec ); $i++, $j += $cl ) {
      if ( $j >= $c ) {
        echo "\n<tr>";
        $j -= $c;
        if     ( $j > 1 ) echo "<td colspan=$j>";
        elseif ( $j     ) echo "<td>";
      }; // if
      $char = mb_substr( $str, $i, 1, $ec );
      $cl = strlen( $char );
      echo $cl > 1 ? "<td colspan=$cl>" : '<td>';
      echo ord( $char ) < 32 ? '[]' : $char;
    }; // for
  } else {
    for ( $i = 0; $i < strlen( $str ); $i++ ) {
      if ( $i % $c == 0 )
        echo "\n<tr>";
      echo '<td>';
      echo ord( $str[$i] ) < 32 ? '[]' : $str[$i];
    }; // for
  }; // if

  echo '</table>
</table>';
  if ( isset( $ec ) )
    echo "\nКодировка: $ec<br />\n";
} // function
?>
